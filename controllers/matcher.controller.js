const { StaticPool } = require('node-worker-threads-pool')
const os = require('os')
const db = require('../models/index.js')
const path = require('path')
const axios = require('axios-https-proxy-fix')
const cheerio = require('cheerio')
const { Op } = require("sequelize");
const c = require('ansi-colors');
const { PythonShell } = require('python-shell')
const log = require('simple-node-logger')
const logger = log.createSimpleFileLogger('project.log')


const Vacancy = db.vacancy
const Company = db.company
const Scammer = db.scammer
const Exception = db.exception
const { dirname, normalize } = require('path');
const appDir = dirname(require.main.filename);
let currentСompany = 0

const indeedWorkerFilePath = path.join(path.dirname(require.main.filename), 'workers', 'worker_indeed.js');
const reedComWorkerFilePath = path.join(path.dirname(require.main.filename), 'workers', 'worker_reed.js');
const numCPUs = os.cpus().length


function ScrapHTML(url) {
  try {
    return new Promise(
      (resolve, reject) => {
        PythonShell.run(normalize(process.env.NODE_ENV === 'development' ? appDir + "/scrapDEV.py" : appDir + "/scrapPROD.py"),
          { args: [url] },
          function (err, results) {
            if (err) {
              reject(err);
            }
            try {
              resolve(results[0]);
            } catch (error) {
              resolve("<html></html>");
            }
            
          }
        );
      }
    );
  } catch (error) {
    console.log("ERROR IN ScrapHTML-", error)
  }
}


//получаем количество страниц для парсинга. Только для ридкома.
async function getPagesCount(agency) {
  try {
    const html = await axios.get(`${agency}`, {
      headers: { 'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/104.0.0.0 Safari/537.36' }
    })
    const $ = cheerio.load(html.data)
    let jobsCount = ""
    $('div.row.search-results > div.col-sm-8.col-md-9.results-container > div.search-results-footer > div:nth-child(1) > div.row-top.hidden-xs > div > div > div').each((i, elem) => {
      jobsCount += `${$(elem).text()}`
    })
    jobsCount = jobsCount.replace(/\s/g, '');
    let start = jobsCount.indexOf("f")
    let end = jobsCount.indexOf("j")
    jobsCount = jobsCount.slice(start + 1, end)
    jobsCount = parseInt(jobsCount)
    let pagesCount = Math.ceil(jobsCount / 25)
    return pagesCount
  } catch (error) {
    console.log(error)
  }
}


async function addCompanies(req, res) {
  console.log(appDir)
  console.log(req.body.companies)
  res.status(200).json({ message: 'Working...' })
  let agencyArr = req.body.companies
  for (let j = 0; j < agencyArr.length; j++) {

    if (agencyArr[j].includes("indeed.com")) {
      try {
        const html = await ScrapHTML(agencyArr[j])
        const $ = cheerio.load(html)
        let companyName = ""
        $('.eu4oa1w0 > div.css-1ce69ph.eu4oa1w0 > div.css-12f7u05.e37uo190 > div.css-1e5qoy2.e37uo190 > div > div').each((i, elem) => {
          companyName += `${$(elem).text()}`
        })
        console.log("ADDING COMPANY NAME: ", companyName)
        const [company, created] = await Company.findOrCreate({
          where: {
            url: agencyArr[j]
          },
          defaults: { companyName, url: agencyArr[j] }
        });
      } catch (error) {
        console.log(c.red(`Error in addCompanies: ${agencyArr[j]} error: ${error}`))
        logger.error(`Error in addCompanies: ${agencyArr[j]} error: ${error}`)
      }
    } else {


      const pageCount = await getPagesCount(agencyArr[j])
      for (let i = 1; i <= pageCount; i++) {
        try {
          const html = await axios.get(`${agencyArr[j]}?pageno=${i}`)
          console.log('add - ', agencyArr[j])
          const $ = cheerio.load(html.data)
          $('div > div.col-sm-12.col-md-9.details > header > h3 > a').each((i, elem) => {
            let cutUrl = `${$(elem).attr('href')}`

            let symbol = cutUrl.indexOf("?")
            cutUrl = cutUrl.substring(0, symbol)
          })
          let companyName = ""
          $('div > div.col-sm-12.col-md-9.details > header > div.job-result-heading__posted-by > a').each((i, elem) => {
            companyName = `${$(elem).text()}`
          })
          const [company, created] = await Company.findOrCreate({
            where: { url: agencyArr[j] },
            defaults: { companyName, url: agencyArr[j] }
          });
        }
        catch (e) {
          console.log(c.red(`Error in addCompanies: ${agencyArr[j]} error: ${e}`))
        }
      }
    }
  }
}


async function findScammersByPhrase(req, res) {

  if (!+process.env.WORKING) {

    let exceptionsArr = []
    const exceptions = await Exception.findAll() //

    Vacancy.destroy({ truncate: { cascade: true } });
    Scammer.destroy({ truncate: { cascade: true } });

    for (let e = 0; e < exceptions.length; e++) {
      exceptionsArr.push(exceptions[e].dataValues.exception.toLowerCase())
    }

    const poolIndeed = new StaticPool({
      size: 6,
      task: indeedWorkerFilePath,
      workerData: 'workerData!'
    });

    const poolReedcom = new StaticPool({
      size: 2,
      task: reedComWorkerFilePath,
      workerData: 'workerData!'
    });

    res.status(200).json({ message: 'Started' })
    process.env.WORKING = 1
    const start = new Date().getTime();
    const companies = await Company.findAll()
    let counter = 0;
    for (let i = 0; i < companies.length; i++) {
      (async () => {
        if (companies[i].url.includes("indeed.com")) {
          let end = companies[i].url.indexOf('.')
          let location = companies[i].url.slice(8, end)
          const indeed = await poolIndeed.exec({ url: companies[i].url, id: companies[i].id, cmpName: companies[i].companyName, location: location, exceptions: exceptionsArr });
        }
        else
          if (companies[i].url.includes("www.reed")) {
            let location = companies[i].url.slice(20, 22)
            const reedcom = await poolReedcom.exec({ url: companies[i].url, id: companies[i].id, cmpName: companies[i].companyName, location: location, exceptions: exceptionsArr });
          }
        currentСompany = i;
        counter++;
        if (counter >= companies.length) {
          await poolIndeed.destroy()
          await poolReedcom.destroy()
          process.env.WORKING = 0
          const end = new Date().getTime();
          console.log(c.green(`END TIME: ${end - start}ms`));
        }
      })();
    }
  } else if (process.env.WORKING) {
    res.status(200).json({ message: "Working..." })
  }
}

async function deleteCompanies(req, res) {
  for (let i = 0; i < req.body.companies.length; i++) {
    try {
      let toDelete = await Company.findOne({
        where: { url: req.body.companies[i] },
      })
      toDelete.destroy()
    } catch (error) {
      console.log(c.red(error));
    }
  }
  res.status(200).json("ALL DONE")
}


async function addExceptions(req, res) {
  for (let i = 0; i < req.body.exceptions.length; i++) {
    const [exception, created] = await Exception.findOrCreate({
      where: {
        exception: req.body.exceptions[i]
      },
      defaults: { exception: req.body.exceptions[i] }
    });
  }
  res.status(200).json({ message: 'Done' })
}



async function getProgres(req, res) {
  const companies = await Company.findAll()
  let progres = (currentСompany / companies.length) * 100
  res.status(200).json({ message: progres + "%" })
}


module.exports = {
  addCompanies, deleteCompanies, findScammersByPhrase, addExceptions, getProgres
}