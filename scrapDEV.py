import sys
import os
import zipfile
from selenium import webdriver
from selenium_stealth import stealth
import time
from selenium.webdriver.chrome.service import Service
url = sys.argv[1]

PROXY_HOST = '138.219.173.110'  # rotating proxy or host
PROXY_PORT = 8000  # port
PROXY_USER = 'WE5Xz4'  # username
PROXY_PASS = 'yB23sc'  # password

manifest_json = """
{
    "version": "1.0.0",
    "manifest_version": 2,
    "name": "Chrome Proxy",
    "permissions": [
        "proxy",
        "tabs",
        "unlimitedStorage",
        "storage",
        "<all_urls>",
        "webRequest",
        "webRequestBlocking"
    ],
    "background": {
        "scripts": ["background.js"]
    },
    "minimum_chrome_version":"22.0.0"
}
"""

background_js = """
var config = {
        mode: "fixed_servers",
        rules: {
        singleProxy: {
            scheme: "http",
            host: "%s",
            port: parseInt(%s)
        },
        bypassList: ["localhost"]
        }
    };

chrome.proxy.settings.set({value: config, scope: "regular"}, function() {});

function callbackFn(details) {
    return {
        authCredentials: {
            username: "%s",
            password: "%s"
        }
    };
}

chrome.webRequest.onAuthRequired.addListener(
            callbackFn,
            {urls: ["<all_urls>"]},
            ['blocking']
);
""" % (PROXY_HOST, PROXY_PORT, PROXY_USER, PROXY_PASS)

s = Service("chromedriver.exe")
def get_chromedriver(use_proxy=False, user_agent=None):
    path = os.path.dirname(os.path.abspath(__file__))
    chrome_options = webdriver.ChromeOptions()

    # if use_proxy:
    pluginfile = 'proxy_auth_plugin.zip'

    # with zipfile.ZipFile(pluginfile, 'w') as zp:
    #     zp.writestr("manifest.json", manifest_json)
    #     zp.writestr("background.js", background_js)
    # chrome_options.add_argument('headless')
    chrome_options.add_experimental_option(
        'excludeSwitches', ['enable-logging'])
    chrome_options.add_extension(pluginfile)
    # chrome_options.add_argument("no-sandbox")
    chrome_options.add_argument("--disable-gpu")
    chrome_options.add_argument("--disable-dev-shm-usage")

    chrome_options.add_argument("no-default-browser-check")
    chrome_options.add_argument("no-first-run")
    # chrome_options.add_argument('--incognito')
    chrome_options.add_argument('log-level=3')
    chrome_options.add_argument("disable-dev-shm-usage")
    chrome_options.add_argument("enable-experimental-web-platform-features")
    # options.add_argument("disable-infobars")  
    # if user_agent:
    #     chrome_options.add_argument('--user-agent=%s' % user_agent)


    driver = webdriver.Chrome(options=chrome_options,
                              service=s)
    return driver

def main():
    try:
        driver = get_chromedriver(use_proxy=True)
        # driver.set_page_load_timeout(15)
        driver.get(url)
        html = driver.page_source
        print(html.encode(encoding="ascii",errors="xmlcharrefreplace"))
    finally:
        driver.close()
        driver.quit()


if __name__ == '__main__':
    main()