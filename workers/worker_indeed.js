const { parentPort } = require('worker_threads')
const axios = require('axios-https-proxy-fix')
const cheerio = require('cheerio')
const db = require('../models/index.js')
const { Op } = require("sequelize");
const c = require('ansi-colors');
const log = require('simple-node-logger')
const { PythonShell } = require('python-shell')
const logger = log.createSimpleFileLogger('project.log')
const Vacancy = db.vacancy
const Company = db.company
const Scammer = db.scammer
const matchCount = 3
const searchType = 1 // 0-new, 1-old
const proxies = ["45.155.201.206"]


const { dirname, normalize } = require('path');
const { search } = require('../routes/matcher.routes.js');
let appDir = dirname(require.main.filename).substring(0, dirname(require.main.filename).length - 7)


function ScrapHTML(url) {
  try {
    return new Promise(
      (resolve, reject) => {
        PythonShell.run(normalize(process.env.NODE_ENV === 'development' ? appDir + "/scrapDEV.py" : appDir + "/scrapPROD.py"),
          { args: [url] },
          function (err, results) {
            if (err) {
              reject(err);
            }
            try {
              resolve(results[0]);
            } catch (error) {
              logger.error(`ERROR INNER ${error}`)
              console.log("", error)
            }
          }
        );
      }
    );
  } catch (error) {
    logger.error(`ERROR OUTER ${error}`)
    console.log("", error)
  }
}

async function getPagesCountParent(agency) {
  let pagesCount = 0
  try {
    const html = await ScrapHTML(`${agency}/jobs?clearPrefilter=1`)
    const $ = cheerio.load(html)

    let isGood = []
    let jobsCount = 0

    isGood = $('li[data-testid="jobListItem"]')

    if (isGood.length) {
      $('.css-18fh2jl.eu4oa1w0 > h2 > span').each((i, elem) => {
        jobsCount += `${$(elem).text()}`
      })
      
      jobsCount = jobsCount.replace(/[^0-9]/g, "")
      jobsCount = parseInt(jobsCount)
      pagesCount = Math.ceil(jobsCount / 150)
      console.log("JOBS COUNT = ", jobsCount)
    } else {
      pagesCount = 0
    }

  } catch (error) {
    logger.error(`getPagesCountParent error OLD:  ${error}`)
  }
  console.log("TRY TO GET PAGE COUNT IN OLD: ", agency)
  console.log("pagesCount OLD: ", pagesCount)

  return pagesCount
}


async function doAll({ url, id, cmpName, location, exceptions }) {
  console.log(c.green("Worker is working..."))
  let idList = []
  let cpmName = url.split("/cmp/")
  // await chooseSearchType(cpmName[1], location)
  let pageCount = await getPagesCountParent(url)
  // let pageCount = await getPagesCountParent(url)
  for (let i = 0; i < pageCount; i++) {
    idList = idList.concat(await getUrlsFromPage(url, i, location))
    console.log("VACANCY TOTAL: ", idList.length)
  }
  let arr = [...new Set(idList)]
  console.log(c.green(`UNIQ VACANCY TOTAL: ${arr.length}`))
  await parseAll(arr, id, location, exceptions, cmpName, url) // сдесь передаем все url вакансий, всех компаний переданных в post запросе
  Company.update(
    {
      unicJobsCount: arr.length,
      declaredJobsCount: idList.length,
    },
    {
      where: { url: url },
    }
  )
}




async function getUrlsFromPage(agency, i) {
  let idList = []
  do {
    try {
      console.log(agency, " PARSING PAGE OLD №: ", i + 1)
      const html = await ScrapHTML(`${agency}/jobs?start=${i * 150}&clearPrefilter=1#cmp-skip-header-mobile`)
      const $ = cheerio.load(html)
      $('div.css-150v3kv.eu4oa1w0 > ul > li').each((i, elem) => {
        let vacancyId = `${$(elem).attr('data-tn-entityid')}`
        var cutId = vacancyId.substring(vacancyId.indexOf(',') + 1, vacancyId.lastIndexOf(','));
        idList.push(cutId)
      })
      console.log(agency, "VACANCY COUNT PARSED ON PAGE OLD №: ", i + 1, " IS: ", idList.length)
    }
    catch (e) {
      console.log("getUrlsFromPage error indeed OLD:", e)
      logger.error(`getUrlsFromPage error indeed OLD:  ${e}`)
    }
  } while (idList.length == 0);
  console.log("VACANCY FROM PAGE: ", idList.length)
  return idList
}


//парсим всё нужное нам содержимое из каждой url массива
async function parseAll(idList, companyId, location, exceptions, cmpName, cmpUrl) {
  for (let i = 0; i < idList.length; i++) {
    console.log("PARSING FROM: ", i, " WHERE ID: ", idList[i])
    try {
      const html = await ScrapHTML(`https://${location}.indeed.com/viewjob?jk=${idList[i]}`)
      const $ = cheerio.load(html)
      let title = ""
      if (location == "www") {
        $('.jobsearch-JobInfoHeader-title').each((i, elem) => {
          title += `${$(elem).text()}` // это идёт в бд
        })
      } else if (location != "www") {
        $('.jobsearch-JobInfoHeader-title-container > h1').each((i, elem) => {
          title += `${$(elem).text()}` // это идёт в бд
        })
      }

       
      let posted = ''
      $('p > span.jobsearch-HiringInsights-entry--text').each((i, elem) => {
        posted += `${$(elem).text()}`
      })

      if (posted.includes('Today') || +posted.replace(/[^\d]/g, '') <= 2) {


        //Удаляем мусор из тайтла и приводим его к необходимому виду
        // пример "Delivery Manager/ Lead", в запросе это должно выглядеть так "Delivery-Manager-Lead"
        let titleForParsing // это идёт в парсинг
        try {
          titleForParsing = title.replace(/[^a-zа-яё0-9-\s]/gi, '').split(' ').filter(Boolean).join('%20') // тонкости языка, тире заменено на %20, тайтл заменен на titleForParsing
        } catch (error) {
          titleForParsing = "" // тайтл заменен на titleForParsing
          console.log("title error indeed", error)
          logger.error(`title error indeed:  ${error}`)
        }
        let description = ""
        $('#jobDescriptionText').each((i, elem) => {
          description += `${$(elem).text()}`
        })
        description = description.replace(/\\n/g, '').replace(/ +/g, ' ').trim();
        let allLi = []
        let uls = []
        try {
          $(`#jobDescriptionText ul`).each((i, elem) => {
            let arrOfLi = []
            $("li", elem).each((j, elem) => {
              let li = `${$(elem).text()}`.trim() //
              // li = li.replace(/\\n/g, '').replace(/ +/g, ' ').trim();
              arrOfLi.push(li)
              allLi.push(li)
            })
            uls.push(arrOfLi)
          })
        } catch (error) {
          logger.error(`1-error indeed: ${error}`)
          console.log("1-error indeed:", error)
        }


        if (uls.length == 0) {
          for (let i = 3; i < 10; i++) {
            try {
              let p = ""
              $(`#jobDescriptionText > div > p:nth-child(${i})`).each((i, elem) => {
                p += `${$(elem).text()}`
                if (p.length > 120 && p.length < 150) {
                  uls.push(p)
                } else if (p.length > 150) {
                  p = p.substring(0, 150).split(' ').filter(Boolean)
                  let removingLastWord = p.splice(-1, 1);
                  p = p.join(' ')
                  uls.push(p)
                }
              })
            } catch (error) {
              logger.error(`2- indeed ${error}`)
              console.log("2- indeed", error)
            }
          }
        }

        let city = ""
        $('.jobsearch-CompanyInfoContainer > div > div > div > div:nth-child(2) > div').each((i, elem) => {
          city += `${$(elem).text()}`

        })

        let logo = ""
        $('.jobsearch-ViewJobLayout-companyPromo > div > div > div.icl-Card-body > a > img').each((i, elem) => {
          logo += `${$(elem).attr('src')}`

        })



        const [vacancy, created2] = await Vacancy.findOrCreate({
          where: {
            url: `https://${location}.indeed.com/viewjob?jk=` + idList[i]
          },
          defaults: { companyId, title, url: `https://${location}.indeed.com/viewjob?jk=` + idList[i], description, location: location == "www" ? "US" : location, city, logo, companyName: cmpName, companyUrl: cmpUrl }
        });
        console.log(c.green("VACANCY SAVED"))

        // console.log("DESCRIPTION - ", description)
        //   console.log("UL - ", uls)
        //   console.log("LI IN UL - ", allLi)



        await parseJobsUrl({
          id: vacancy.id, title: titleForParsing, url: idList[i],   // тайтл заменен на titleForParsing
          description: description,
          city: city, logo: logo,
          cmpName: cmpName,
          cmpUrl: cmpUrl

        }, location, uls, allLi, exceptions)



      }





    } catch (e) {
      console.log(c.red(`VACANCY NOT SAVED error indeed: ${e}`))
      logger.error(`VACANCY NOT SAVED error indeed:  ${e}`)
    }
  }
}

function chunkArray(myArray, chunk_size) {
  let results = [];
  while (myArray.length) {
    results.push(myArray.splice(0, chunk_size));
  }
  return results;
}

//парсим url всех потенциальных скамеров, закидываем их в массив  obj = 1 вакансия
async function parseJobsUrl(obj, location, uls, allLi, exceptions) {
  let potentialScammerList = []
  let queries = []
  let url = `https://${location}.indeed.com/jobs?q=`
  if (uls.length) {
    uls.forEach(ul => {
      // получаю списки ul = отдельный список / li
      // берём список, чанкаем его, по 5 и через энкод ури запихиваем в запрос по 5 елементов
      chunkArray(ul, 5).forEach(chunk => {
        queries.push(url + encodeURI(chunk.join("")))
      })
    });

    try {
      for (let j = 0; j < queries.length; j++) {
        try {
          console.log(queries[j])
          const html = await ScrapHTML(`${queries[j]}`)
          const $ = cheerio.load(html)
    
          $('div > div.slider_item.css-kyg8or.eu4oa1w0 > div > table.jobCard_mainContent.big6_visualChanges > tbody > tr > td > div.css-1m4cuuf.e37uo190 > h2 > a').each((i, elem) => {

            let vacancyId = `${$(elem).attr('href')}`
            let cutId = vacancyId.substring(vacancyId.indexOf('=') + 1, vacancyId.indexOf('&'));
            if (cutId.length > 16) {
              cutId = cutId.substring(cutId.lastIndexOf('-') + 1, cutId.lastIndexOf('?'))
            }
            potentialScammerList.push(cutId)
          })
          console.log(c.blue(`POTENTIAL SCAMER LIST IS: ${potentialScammerList.length}`))

        } catch (error) {
          logger.error(`parseJobsUrl title IN CYCLE: ${obj.title}, error indeed: ${error}`)
          console.log(c.red(`ParseJobsUrl error indeed indeed: ${error}`))
        }
      }
    } catch (error) {
      console.log(c.red(`Out of page range error indeed: ${error}`))
      logger.error(`parseJobsUrl OUT OF CYCLE error indeed: ${error}`)
    }
    await parseAllFrompotentialScammer([...new Set(potentialScammerList)], obj, url, location, allLi, exceptions)
  }
}


//парсим всё нужное нам содержимое  из каждой url массива потенциальных скамеров
async function parseAllFrompotentialScammer(urls, obj, url, location, allLi, exceptions) {
  console.log(urls)
  for (let i = 0; i < urls.length; i++) {
    try {

      const html = await ScrapHTML(`https://${location}.indeed.com/viewjob?jk=${urls[i]}`)
      const $ = cheerio.load(html)

      let title = ""
      $('div.jobsearch-JobInfoHeader-title-container > h1').each((i, elem) => {
        title += `${$(elem).text()}`
      })
      console.log(c.yellow(`TITLE:  ${title}`))

      let description = ""
      $('#jobDescriptionText').each((i, elem) => {
        description += `${$(elem).text()}`
      })
      description = description.replace(/\\n/g, '').replace(/ +/g, ' ').trim();

      let companyName = ""
      $('.jobsearch-DesktopStickyContainer-companyrating > div:nth-child(2) > div > a').each((i, elem) => {
        companyName += `${$(elem).text()}`
      })
      if (companyName == "")
        $('.jobsearch-DesktopStickyContainer-companyrating > div:nth-child(2) > div').each((i, elem) => {
          companyName += `${$(elem).text()}`
        })

      console.log(c.yellow(`COMPANY NAME: ${companyName}`))

      let companyUrl = ""
      $('.jobsearch-DesktopStickyContainer-companyrating > div:nth-child(2) > div > a').each((i, elem) => {
        companyUrl += `${$(elem).attr('href')}`
      })
      console.log(c.yellow(`COMPANY URL: ${companyUrl}`))

      let city = ""
      $('.jobsearch-CompanyInfoContainer > div > div > div > div:nth-child(2) > div').each((i, elem) => {
        city += `${$(elem).text()}`
      })
      console.log(c.yellow(`CITY: ${city}`))

      let logo = ""
      $('.jobsearch-ViewJobLayout-companyPromo > div > div > div.icl-Card-body > a > img').each((i, elem) => {
        logo += `${$(elem).attr('src')}`
      })
      console.log(c.yellow(`LOGO: ${logo}`))

      let liCountMatch = 0
      let liMatch = []
      for (let li = 0; li < allLi.length; li++) {
        if (description.toLowerCase().includes(allLi[li].toLowerCase()) && !exceptions.includes(allLi[li].toLowerCase())) {
          liCountMatch++
          liMatch.push(allLi[li])
        }
      }
      //&& !companyName.includes(obj.cmpName)
     let agencyCheck = await Company.findOne({ where: { companyName: companyName } })
      if (liCountMatch >= matchCount && !companyName.includes(obj.cmpName) && obj.url !== urls[i] && !agencyCheck) {
        console.log(c.red('SCAMMER!!!!!!!!!!!!!!!!!!!!!!!!!!'));
        const [scammer, created] = await Scammer.findOrCreate({
          where: {
            [Op.or]: [
              { url: `https://${location}.indeed.com/viewjob?jk=` + urls[i] },
              // { companyName: companyName }
            ]
          },
          defaults: { vacancyId: obj.id, url: `https://${location}.indeed.com/viewjob?jk=` + urls[i], companyName, jobTitle: title, description, location: location == "www" ? "US" : location, city, logo, query: url }
        });

        if (process.env.NODE_ENV === 'production') {
          await sendSCAM('https://matcher2.bubbleapps.io/api/1.1/wf/matcher_indeed', obj, location, url, liMatch, urls[i], companyName, title, city, logo)
          await sendSCAM('https://hrprime.bubbleapps.io/api/1.1/wf/matcher_indeed', obj, location, url, liMatch, urls[i], companyName, title, city, logo)
        }
      }
    }
    catch (e) {
      logger.error(`parseAllFrompotentialScammer: url ${urls[i]}, error indeed: ${e}`)
      console.log(c.red(`parseAllFrompotentialScammer error indeed: ${e} on url ___:${urls[i]}`))
    }
  }
}


async function sendSCAM(endPoint, obj, location, url, liMatch, currentUrl, companyName, title, city, logo) {
  try {
    if (liMatch.join(" ").length >= 140) {
      const post = await axios.post(endPoint, {
        "source": 'indeed',
        "clientName": obj.cmpName,
        "clientUrl": obj.cmpUrl,
        "parentVacancyName": obj.title,//
        "parentVacancyUrl": `https://${location}.indeed.com/viewjob?jk=` + obj.url,
        "parentVacancyLocation": location == "www" ? "US" : location.toUpperCase(),
        "parentVacancyCity": obj.city, //
        "parentCompanyLogo": obj.logo, //
        "query": url, //
        "matchingLines": liMatch,
        "scammerVacancyUrl": `https://${location}.indeed.com/viewjob?jk=` + currentUrl,
        "scammerCompanyName": companyName,
        "scammerJobTitle": title,
        "scammerJobLocation": location == "www" ? "US" : location.toUpperCase(),
        "scammerJobCity": city,//
        "scammerCompanyLogo": logo,//
      })
    }
  } catch (error) {
    logger.error(`Error axios.post to ${endPoint} : error indeed: ${error}`)
  }
}



// Основной поток передаст нужные вам данные
// через этот прослушиватель событий.
parentPort.on('message', async (param) => {
  const result = await doAll(param);
  // Access the workerData.
  // return the result to main thread.
  parentPort.postMessage(`feels good`);
});
