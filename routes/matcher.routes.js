const { Router } = require('express')
const matcherController = require('../controllers/matcher.controller')
const router = Router()

// '/api/matcher'
router.post('/addCompanies', matcherController.addCompanies)
router.get('/findScammersByPhrase', matcherController.findScammersByPhrase)
router.post('/deleteCompanies', matcherController.deleteCompanies)
router.post('/addExceptions', matcherController.addExceptions)
router.get('/getProgres', matcherController.getProgres)

module.exports = router
